<!DOCTYPE html>
<html>
<head><title>League table </title>
 <link href="leaguetable.css" rel="stylesheet"> 
</head>
<div class="ptable">
  <h1 class="headin">Standings</h1>
					<table>
						<tr class="col">
							<th>#</th>
							<th>Team</th>
							<th>GP</th>
							<th>W</th>
							<th>D</th>
							<th>L</th>
							<th>GD</th>
							<th>PTS</th>
						</tr>
						<tr class="wpos">
							<td>1</td>
							<td>Manchaster City</td>
							<td>14</td>
							<td>12</td>
							<td>2</td>
							<td>0</td>
							<td>37</td>
							<td>38</td>
						</tr>
						<tr class="wpos">
							<td>2</td>
							<td>Liverpool FC</td>
							<td>14</td>
							<td>2</td>
							<td>0</td>
							<td>0</td>
							<td>4</td>
							<td>6</td>
						</tr>
						<tr class="wpos">
							<td>3</td>
							<td>Chelsea FC</td>
							<td>14</td>
							<td>1</td>
							<td>1</td>
							<td>0</td>
							<td>4</td>
							<td>4</td>
						</tr>
						<tr class="wpos">
							<td>4</td>
							<td>Arsenal FC</td>
							<td>14</td>
							<td>1</td>
							<td>1</td>
							<td>0</td>
							<td>1</td>
							<td>4</td>
						</tr>
						<tr class="pos">
							<td>5</td>
							<td>Tottenham</td>
							<td>14</td>
							<td>1</td>
							<td>0</td>
							<td>1</td>
							<td>0</td>
							<td>3</td>
						</tr>
						<tr class="pos">
							<td>6

                            </td>
							<td>Everton</td>
							<td>14</td>
							<td>1</td>
							<td>0</td>
							<td>2</td>
							<td>-1</td>
							<td>3</td>
						</tr>
						<tr class="pos">
							<td>7</td>
							<td>Manchaster United</td>
							<td>14</td>
							<td>1</td>
							<td>0</td>
							<td>1</td>
							<td>-2</td>
							<td>3</td>
						</tr>
						<tr class="pos">
							<td>8</td>
							<td>Leicester</td>
							<td>14</td>
							<td>0</td>
							<td>1</td>
							<td>0</td>
							<td>0</td>
							<td>1</td>
						</tr>
						<tr class="pos">
							<td>9</td>
							<td>Bournemouth </td>
							<td>14</td>
							<td>0</td>
							<td>1</td>
							<td>1</td>
							<td>-1</td>
							<td>1</td>
						</tr>
						<tr class="pos">
							<td>10</td>
							<td>Watford</td>
							<td>14</td>
							<td>0</td>
							<td>0</td>
							<td>2</td>
							<td>-4</td>
							<td>0</td>
						</tr>
						<tr class="pos">
							<td>11</td>
							<td>Brighton</td>
							<td>14</td>
							<td>0</td>
							<td>0</td>
							<td>2</td>
							<td>-5</td>
							<td>0</td>
                        </tr>
                        
                        <tr class="pos">
                                <td>12</td>
                                <td>Wolves</td>
                                <td>14</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                            </tr>

                         <tr class="pos">
                              <td>13</td>
                                <td>West Ham</td>
                                <td>14</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>                                   
                                <td>-5</td>
                                <td>0</td>
                                </tr>

                        <tr class="pos">
                                <td>14</td>
                                <td>Crystal Palace</td>
                                <td>14</td>                                       
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                                </tr>
  
                         <tr class="pos">
                                <td>15</td>
                                <td>Newcastle</td>
                                <td>14</td>
                                <td>0</td> 
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                                </tr>

                        <tr class="pos">
                                 <td>16</td>
                                 <td>Cardiff</td>
                                 <td>14</td>
                                 <td>0</td>
                                 <td>0</td>
                                 <td>2</td>
                                 <td>-5</td>
                                 <td>0</td>
                                 </tr>

                        <tr class="pos">
                                 <td>17</td>
                                 <td>Huddersfield</td>
                                 <td>14</td>
                                 <td>0</td>
                                 <td>0</td>
                                 <td>2</td>
                                 <td>-5</td>
                                 <td>0</td>
                                 </tr>

                        <tr class="pos">
                                <td>18</td>
                                <td>Southampton</td>
                                <td>14</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                                </tr>
                        
                        <tr class="pos">
                                <td>19</td>
                                <td>Burnley</td>
                                <td>14</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                                </tr>        

                        <tr class="pos">
                                <td>20</td>
                                <td>Fulham</td>
                                <td>14</td>
                                <td>0</td>
                                <td>0</td>
                                <td>2</td>
                                <td>-5</td>
                                <td>0</td>
                                </tr>
					</table>
</div>